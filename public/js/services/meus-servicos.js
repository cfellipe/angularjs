angular.module("meusServicos", ['ngResource'])
    .factory('recursoFoto', function ($resource) {

    return $resource('/v1/fotos/:fotoId', null, {
        'update': {
            method: 'PUT'
        }
    });
}).factory("cadastroDeFotos", function(recursoFoto, $q, $rootScope){

    var service = {};
    var evento ='fotoCadastro'

    service.cadastrar = function(foto){

        return $q(function(resolve, reject){

            $rootScope.$broadcast(evento);
            if(foto._id){
               
                recursoFoto.update({fotoId: foto._id}, foto, function(){

                    resolve({
                        mensagem: 'Foto atualizada com sucesso',
                        inclusao: false
                    });
                }, function(erro){

                    console.log(erro);
                    reject({
                        mensagem:'Não foi possível atualizar a foto'+ foto.titulo
                    });
                });

            }else{
                
                
                recursoFoto.save(foto, function(){

                    resolve({
                        mensagem: 'Foto cadastrada com sucesso',
                        inclusao: true
                    });
                }, function(erro){

                    console.log(erro);
                    reject({
                        mensagem:'Não foi possivel realizar o cadastro de fotos'
                    });
                });

            }
        });
    }
    return service;
})